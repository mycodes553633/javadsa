
import java.util.*;
class ArrayListDemo{
	public static void main(String[] args){
		ArrayList al=new ArrayList();
		al.add(10);
		al.add(20.5);
		al.add(30.5f);
		al.add("c2w");

		Vector v=new Vector();
		v.addElement(10);
		v.addElement(20);
		v.addElement(30);
		v.addElement(40);

		for(var x:al){
			System.out.println(x.getClass().getName());
		}
		
		//itertor
		Iterator cursor=al.iterator();
		System.out.println(cursor.getClass().getName());
		while(cursor.hasNext()){
			System.out.println(cursor.next());
		  //	cursor.remove();
		}
		System.out.println(al);

		//listitertor
		ListIterator litr=al.listIterator();
		System.out.println(litr.getClass().getName());
		while(litr.hasNext()){
			System.out.println(litr.next());
		}
		while(litr.hasPrevious()){
			System.out.println(litr.previous());
		} 

		//Enumeration
		
		Enumeration cursor1=v.elements();
		System.out.println(cursor1.getClass().getName());
		while(cursor1.hasMoreElements()){
			System.out.println(cursor1.nextElement());
		}



	}
}



